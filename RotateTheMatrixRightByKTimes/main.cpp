#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> createSingleRowFromArrayStartingWithIndexWithWidth(int arr[] , int startIndex , int width)
        {
            vector<int> singleRow;
            for(int i = startIndex ; i < (startIndex + width) ; i++)
            {
                singleRow.push_back(arr[i]);
            }
            return singleRow;
        }
    
        vector< vector<int> >initializeVectorFromArrayWithRowColoumnSizes(int arr[] , int rowSize , int coloumnSize)
        {
            vector< vector<int> > matrixVector;
            for(int i = 0 ; i < coloumnSize ; i++)
            {
                matrixVector.push_back(createSingleRowFromArrayStartingWithIndexWithWidth(arr , (i * rowSize) , rowSize));
            }
            return matrixVector;
        }
    
        int getReplacingIndexOfIndex(int index , int k , int rowSize)
        {
            return (index - k >= 0 ? index - k : rowSize - (k - index));
        }
    
    public:
        vector< vector<int> > rotateArrayWithRowAndColoumnSizesRightByKTimes(int arr[] , int rowSize , int coloumnSize , int k)
        {
            vector< vector<int> > matrixVector        = initializeVectorFromArrayWithRowColoumnSizes(arr , rowSize , coloumnSize);
            int                   currentColoumn      = 0;
            int                   toBeReplacedColoumn = getReplacingIndexOfIndex(currentColoumn , k , rowSize);
            int                   tempArr[10]         = {0};
            
            for(int j = 0 ; j < coloumnSize ; j++)
            {
                tempArr[j] = matrixVector[j][currentColoumn];
            }
            
            for(int i = 0 ; i < rowSize ; i++)
            {
                for(int j = 0 ; j < coloumnSize ; j++)
                {
                    int temp                             = matrixVector[j][toBeReplacedColoumn];
                    matrixVector[j][toBeReplacedColoumn] = tempArr[j];
                    tempArr[j]                           = temp;
                }
                currentColoumn      = toBeReplacedColoumn;
                
                toBeReplacedColoumn = getReplacingIndexOfIndex(currentColoumn , k , rowSize);
            }
            return matrixVector;
        }
    
        void displayVector(vector< vector<int> > displayingVector)
        {
            int rowSize = (int)displayingVector.size();
            int colSize = (int)displayingVector[0].size();
            for(int i = 0 ; i < rowSize ; i++)
            {
                for(int j = 0 ; j < colSize ; j++)
                {
                    cout<<displayingVector[i][j]<<"  ";
                }
                cout<<endl;
            }
        }
};

int main(int argc, const char * argv[])
{
    int                   arr[]         = {1 , 2 , 3 , 4};//{1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12 , 13 , 14 , 15 , 16 , 17 , 18 , 19 , 20};//{12 , 23 , 34 , 45 , 56 , 67 , 78 , 89 , 91};
    int                   rowSize       = 2;//5;//3;
    int                   coloumnSize   = 2;//4;//3;
    int                   kTimes        = 2;//3;//2;
    Engine                e             = Engine();
    vector< vector<int> > rotatedVector = e.rotateArrayWithRowAndColoumnSizesRightByKTimes(arr , rowSize , coloumnSize , kTimes);
    e.displayVector(rotatedVector);
    return 0;
}
